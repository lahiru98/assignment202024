#include <stdio.h>
int main()
{
    char chtr;

    printf("Enter a character: ");
    scanf("%c", &chtr);


    if(chtr=='a' || chtr=='e' || chtr=='i' || chtr=='o' || chtr=='u' ||
       chtr=='A' || chtr=='E' || chtr=='I' || chtr=='O' || chtr=='U')
        {
        printf("'%c' is a Vowel", chtr);
        }

    else
        {
        printf("'%c' is a Consonant", chtr);
        }

    return 0;
}
