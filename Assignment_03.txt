#include <stdio.h>
int main ()
{
    int num1, num2, temp;

    printf(" Enter 1st number: ");
    scanf("%d", &num1);
    printf(" Enter 2nd number: ");
    scanf("%d", &num2);

    temp = num1;
    num1 = num2;
    num2 = temp;

    printf("\n After swapping, 1st number: %d\n", num1);
    printf(" After swapping, 2nd number: %d", num2);
    return 0;
}
