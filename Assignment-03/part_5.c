#include <stdio.h>
int main ()
{
    int num, m=0;
    printf("Input the number: ");
    scanf("%d", &num);
    printf("%4c", 'X');

    for(int i=1; i <=num; i++)
        {
        printf("%6d", i);
        }
    printf("\n");

    for(int i=0; i <= 6*num+4; i++, printf( "_" ));
    printf("\n");

    for(int i=1; i<=num; i++)
    {
        printf("%3d|", i);
        for(int j=1; j<=num; j++)
            {
            printf("%6d", j*i);
            }
        printf("\n");
    }
    return 0;
}
